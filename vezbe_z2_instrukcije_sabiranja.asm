PROGRAM "Instrukcije sabiranja"
DATA
VAR1 DB 0X1
VAR2 DB 0XA
RES1 DB 0X1
RES2 DB 0X0
RES3 DB 0X0
RES4 DB 0X0
ENDDATA
CODE
MOV A, 0X1   	
ADD A, 0X1	; Prethodna ucitana vr. u akumulator, 0x1 biva uvecava za 0x1
MOV RES1, A	; Prebacujemo A = 0x2 u RES2 promjenjivu.
MOV AP, 0X5   	; Upisujem 5 u AP,
ADD AP, 0X3	; Dodajem 3 na prethodnu vr. AP, znaci 5+3 --> AP = 8
MOV RES2, AP
ADD A, VAR1
MOV RES3, A
MOV AP,@VAR1	; Upisujem adresu od VAR1 u AP
ADD A, [AP]	; Na vrednost akumulatora dodajem vr. sa memorijske lokacije na
		; adresi koja se nalazi u registru AP (ISPRATITI VREDNOST A...)
MOV RES4, A
END
ENDPROGRAM