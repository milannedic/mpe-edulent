PROGRAM"Instrukcije prenosa podataka"
DATA
VAR1 DB 0X44
VAR2 DB 0XF
RES1 DB 0X0
RES2 DB 0X0
RES3 DB 0X0
ENDDATA
CODE
MOV A, 0x1	; Akumulator dobija vrijednost konstante, 0x1
MOV A, VAR1	; Akumulator dobijas vrijednst VAR1 = 0x44
MOV AP, @VAR1	; Adresni pokazivac dobija adresu od VAR1, @VAR1 = 0x12
MOV A, [AP]	; A dobija vr. memorijske lokacije na adresi koju cuva AP
		; a posto je AP prethodno dobio @VAR1, to znaci da je A = 0x12
		; (pogledati desno memoriju i zadrzati mis preko lokacije 0x12)
		
MOV AP, 0XAA	; AP dobijas vrijednost konstante 0xAA
MOV RES1, AP	; RES1 postaje AP, a to je 0xAA
MOV AP, VAR2	; AP dobija vrijednost VAR2
MOV RES2, AP	; RES2 postaje AP
MOV RES3, A	; RES3 	postaje A
END		; KRAJ PROGRAMA...
ENDPROGRAM