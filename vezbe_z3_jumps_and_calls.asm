PROGRAM "Jumps and calls"
DATA
RES1 DB 0X0
ENDDATA
CODE
	MOV A, 0X3
L1: CALL DEC ;dekrement
		; VAZNO! Ovo pratiti uz tabelu instrukcije CALL procedure!
		;
		; Pri svakom pozivu CALL instrukcije izvrsava se
		; ukupno 10 mikroinstrukija! Najveca instrukcija Edulenta.
		; Prvih pet ciklusa su isti kao i kod nekih ostalih instrukcija
		; T0 - T2 Prihvat instrukcije
		; T3 - T4 Prihvat adrese
		; T5 AP <-- MD, posto smo prethodno prihvatili adresu gdje
		; 		zelimo izvrsiti skok, sada  se u MD nalazi
		;		adresa lokacije na koju skacemo i tu adresu prepisujemo
		;		u AP, adresni pointer  registar, bas tome i sluzi
		;		u njemu po potrebi privremeno cuvamo neku adresu
		; SP<--SP-1   	SP = Stack Pointer, pokazivac steka sluzi 
		;		da bi pokazao na vrh steka, stek je dio memorije
		;		i kod ovog mikroprocesora je smjesten na kraj 
		;		adresnog prostora. STEK je kako znamo sa PJISP
		;		struktura podataka koja dozvoljava samo citanje 
		;		sa vrha, zamislimo ga kao slozene tanjire
		;		gdje mozemo sve da ih uzmemo, ali je dozvoljeno
		;		samo s vrha da ih uzimamo, znaci onaj koji je 
		;		postavljen poslednji mora da se uzmi prvi!
		;		Posto je stek pointer u normalnom rezimu rada,
		;		kada ne pravimo nikakve skokove na vrijednosti 0x00
		;		koja je naredna posle 0x7F koliko mi maksimalno imamo
		;		memorije, to znaci da stek nije uopste inicijalizovan.
		;		Da bismo znali gdje da se vratimo u programu kada izvrsimo
		;		zeljenu radnju proceduri, CUVAMO adresu gdje smo bili
		;		prije skoka, i to bas na STEK! Zato smo odradili umanjenje
		;		pokazivaca steka koji nakon 0x00 vrijednosti dobija vrijednost
		;		0x7F sto je poslednja memorijska lokacija i ujedno SAMI VRH STEKA!
		;		Znaci na vrh steka smo sacuvali adresu gdje treba da se vratimo!
		;
		; .... DOVRSITI
		
	JZ L2
	JMP L1
L2: CALL INC ;inkrement
	MOV RES1, A
END

PROCEDURE INC
	ADD A,0X1
	RET
ENDPROCEDURE

PROCEDURE DEC
	SUB A,0X1
	RET
ENDPROCEDURE

ENDPROGRAM 