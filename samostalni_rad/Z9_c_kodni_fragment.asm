PROGRAM "C kodni fragment if(i==j)"
DATA
	F 	DB 0x0  
	G 	DB 7
	H 	DB 3
	i	DB 6
	j	DB 5
ENDDATA
CODE
	
LMAIN:	CALL CHECKEQUAL
	JZ LADD   		; ako nemmam ZERO FLAG, znaci da nisu isti
				; skacem bezuslovno na oduzimanje i izlazim iz
				; programa
	JMP LSUB

LADD:  CALL ADDGH
	JMP LEND
	
LSUB: CALL SUBGH
	JMP LEND		; zavrsavam program
	
LEND:	END

PROCEDURE CHECKEQUAL
	MOV A, i	; G u akumulator
	SUB A, j	 
	RET
ENDPROCEDURE

PROCEDURE ADDGH      	; G + H
	MOV A, G
	ADD A, H
	MOV F, A	; upisi rezultat sabiranja
	RET
ENDPROCEDURE

PROCEDURE SUBGH      	; G - H
	MOV A, G
	SUB A, H
	MOV F, A	; upisi rezultata oduzimanja
	RET
ENDPROCEDURE
ENDPROGRAM