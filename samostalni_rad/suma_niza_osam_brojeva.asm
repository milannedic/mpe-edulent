Program "Suma niza"
Data
Var1 db 2
Var2 db 1
Var3 db 1
Var4 db 1
Var5 db 1
Var6 db 1
Var7 db 1
Var8 db 1
Sum db 0
Brojac db 7
Enddata

Code
Mov A, Var1
Mov Sum, A

Mov AP, @Var1

Loop: Add AP, 0x01
Mov A, Sum
Add A, [AP]
Mov Sum, A
Mov A, Brojac
Sub A, 1
Mov Brojac, A

JZ Lend
Jmp Loop

Lend: End
Endprogram