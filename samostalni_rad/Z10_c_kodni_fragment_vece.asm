PROGRAM "10. C kodni fragment if(i > j)"
DATA
	F 	DB 0x0  
	G 	DB 8
	H 	DB 5
	i	DB 6
	j	DB 5
ENDDATA
CODE	
; if(i > j) f = g + h
; else
; f = g - h
LMAIN:	CALL I_MINUS_J
; ako je I vece od J znaci da treba da ih SABEREM
; ako je I manje od J ili I == J, tada ih oduzimam
	JZ LSUB
	JC LSUB  		; pri oduzimanju VECEG od MANJEG aktivira se CARRY		
				; nista prethodno nije zadovoljeno znaci da je 
	JMP LADD		; uslov > zadovoljen --> SABERI IH
				; LSUB i LADD imaju skok na LEND na kraju...

LADD:  CALL ADDGH
	JMP LEND
	
LSUB: CALL SUBGH
	JMP LEND		; zavrsavam program
	
LEND:	END

;--------------------------------------------
PROCEDURE I_MINUS_J
	MOV A, i	; i u akumulator
	SUB A, j	; oduzmi j 
	RET
ENDPROCEDURE
PROCEDURE ADDGH      	; G + H
	MOV A, G
	ADD A, H
	MOV F, A	; upisi rezultat sabiranja
	RET
ENDPROCEDURE
PROCEDURE SUBGH      	; G - H
	MOV A, G
	SUB A, H
	MOV F, A	; upisi rezultata oduzimanja
	RET
ENDPROCEDURE
ENDPROGRAM