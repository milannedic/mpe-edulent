PROGRAM "Broj nula i jedinica"
DATA
	X 	DB 0x1F  ; 0001 1111   daje rezultat ONES = 5, ZEROS = 3
	DIGCNT  DB 0x8	; svi registri su osmobitni tako da imam max 8 cifara 
	ONES 	DB 0
	ZEROS 	DB 0
	ACC     DB 0	; prethodni sadrzaj akumulatora, pomocna promenjiva
ENDDATA
CODE
	MOV A, X	
LMAIN:	SHR   		; glavni deo
	MOV ACC, A  	; cuvam prethodni sadrzaj akumulatora
	JC LINC1   	; 
	;JZ LEND
	JZ LZEROSCNT 	; SKOCI NA LABELU ZA RACUNANJE BROJA NULA
			; KOJA CE NAKON POVRATKA IZ PROCEDURE IZACI IZ PROGRAMA
	JMP LMAIN   	; ovde ce doci ako  je prosao i JC i JZ - nisu izvrseni
			; ako ne napisem JMP LMAIN on ce preskociti odmah dalje na LINC1:
			; nista ga ne sprecava da skoci 
LINC1:  CALL INC
	JMP LMAIN

LRETRES: MOV A, ZEROS

LZEROSCNT: CALL CALCZEROS
	;JMP LMAIN  -- nije potreban ovaj korak, cim izadje iz procedure
	;           -- nastavice dalje da izvrsava i uci ce u LEND:
LEND:	END
PROCEDURE INC      	; inkrement
	MOV A, ONES
	ADD A, 0x1
	MOV ONES, A
	MOV A, ACC
	RET
ENDPROCEDURE
PROCEDURE CALCZEROS    	; izracunava broj nula
	MOV A, DIGCNT
	SUB A, ONES
	MOV ZEROS, A
	RET
ENDPROCEDURE
ENDPROGRAM