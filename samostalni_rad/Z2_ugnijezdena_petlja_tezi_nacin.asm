PROGRAM "Petlja 16 puta"
; Napisati program koji realizuje dve 
; (ugnezdene) petlje od kojih se svaka ponavlja x
; puta (ukupno x * x prolaza)

; C-ovski ekvivalent je:
;
;for (i = 0; i < 5; i++) {
;	for(j = 0; j < 5; j++){
;		prom++;
;	}
;}
DATA
   M DB 4 ; granica I petlje
   N DB 4 ; -- || -- J petlje
   I DB 0
   J DB 0
   T_J DB 0	; promenjiva 0 ili 1 znaci TACNO ili NETACNO
   T_I DB 0
   PROM DB 0 ; pomocna promenjiva	
ENDDATA
CODE

LINC_I:	MOV A, I
	ADD A, 1
	MOV I, A    ; umanjio sam I i cuvam vrednost 
LINC_J:	MOV A, J
	ADD A, 1
	MOV J, A	; spremim uvecano J 
	MOV A, PROM
	ADD A, 1	; uvecam PROM
	MOV PROM, A	; spremim uvecano PROM
	
	CALL CHECKEQUAL_J
	MOV A, T_J  	; upisem T nakon povratka u MAIN PROGRAM
			; ako je njegova vrednost nula
			; znaci nisam stigao do kraja petlje
			; pa se vracam nazad i uvecavam petlju J
	ADD A, 0	; BUDZENJE JER MOV INSTRUKCIJA NE UTICE NA INDIKATORE
	JZ LINC_J  ; sve dok je T = 0 znaci nisam doterao do kraja petlje
		    	; ako je ipak nula bice true, skacemo na I petlju
	; ----------------------------------------------------
 	; AKO SAM PROSAO OVDE, ZNACI DA JE ZAVRSENA J PETLJA JEDANPUT!
	; Ponistavam T_J
	SUB A, 1
	MOV T_J, A
	
	MOV A, 0
	MOV J, A	; ponistavam J petlju, jer treba ponovo da broji
	
 	CALL CHECKEQUAL_I
 	MOV A, T_I
 	ADD A, 0 	; PONOVO BUDZENJE
 	JZ LINC_I
	
LEND:	END

PROCEDURE CHECKEQUAL_J
	MOV A, N
	SUB A, J
	
	SUB A, 1        ; umanjim akumulator jos za jedan
			; ako je 0 znaci da sam stigao bas do
			; kraja  odnosno I < M je poslednji put 
			; tacno
	JZ L_T_TRUE_J
	
	JMP L_END_CHCK_J
	
	
L_T_TRUE_J:   	MOV A, 1
		MOV T_J, A
L_END_CHCK_J: RET
ENDPROCEDURE

PROCEDURE CHECKEQUAL_I
	MOV A, M
	SUB A, I
	
	SUB A, 1        ; umanjim akumulator jos za jedan
			; ako je 0 znaci da sam stigao bas do
			; kraja  odnosno I < M je poslednji put 
			; tacno
	JZ L_T_TRUE_I
	
	JMP L_END_CHCK_I
	
	
L_T_TRUE_I:   	MOV A, 1
		MOV T_I, A 
L_END_CHCK_I: RET
ENDPROCEDURE
ENDPROGRAM