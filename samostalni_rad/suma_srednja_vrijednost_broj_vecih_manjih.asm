PROGRAM "Suma"
;// Dat je niz od 8 elemenata, izracunati srednju vrednost njihovu i
;// prebrojati koliko ih je manje od 5, koliko je vece od 5 i koliko je jednako 5
;
;int main(){
;	int niz[8];
;	int avg = 0;
;	int manjih = 0;
;	int vecih = 0;
;	int jednakih = 0;
;	int i = 0; // brojac
;
;	for(i = 0; i < 8; i++){
;		if(niz[i] < 5) {
;			manjih++;
;		}
;
;		if(niz[i] > 5) {
;			vecih++;
;		} else {
;			jednakih++;
;		}
;	}
;}
;
;// nisam kompajlirao ovaj dio pa ne znam da li radi, posmatrati kao
;// pseudo-code, stavise ne bi ni radio, nisam nigde upisao
;// elemente u niz, zamisliti da je niz napunjen.
DATA
	VAR1 DB 4
	VAR2 DB 2
	VAR3 DB 7
	VAR4 DB 5
	VAR5 DB 5
	VAR6 DB 7
	VAR7 DB 0
	VAR8 DB 6

	SUM DB 0
	BROJAC DB 8
	AVG DB 0 ; srednja vrednost
	MANJIH DB 0
	VECIH DB 0
	JEDNAKIH DB 0

	; VECIH = 2
	; MANJIH = 4
	; JEDNAKIH = 2

ENDDATA
CODE

	MOV AP, @VAR1 	; @ uzima adresu promenjive , &niz
 			        ; sluzi kao pocetna tacka programa
LOOP:	MOV A, SUM
	ADD A, [AP]
	                ;ADD A, [AP]	; u AP imam adresu 2, 3, 4... elementa
			        ; [] kazu IDI NA TU ADRESU I UZMI VREDNOST SA NJE
	MOV SUM, A 	    ; vracam rezultat obrade u SUM, presipam iz akumulatora
			        ; Dakle, sadrzaj registra A prebaci u SUM

	; -------------------------------------
	; ---- provjeravamo DA LI JE BROJ == 0
	MOV A, [AP]  	; [AP] cuva VREDNOST aktuelnog clana niza na kom se
			        ; nalazi petlja u trenutnom prolazu niz[i] C-ovski gledano
	SUB A, 5
	JZ L_INC_JEDNAKIH
	JC L_INC_MANJIH	; pri oduzimanju VECEG od MANJEG aktivira se CARRY
			        ; pri oduzimanju VECEG od MANJEG aktivira se CARRY
		         	; pri oduzimanju VECEG od MANJEG aktivira se CARRY
			        ; pri oduzimanju VECEG od MANJEG aktivira se CARRY

; ako NIJE jednak, i ako nije MANJI onda je sigurno veci od 10
; stoga samo uvecavam promenjivu koja cuva broj VECIH
L_INC_VECIH: MOV A, VECIH
	ADD A, 1
	MOV VECIH, A

; ------------ ispod je zavrsni dio petlje koji se vracaju provjere MANJIH i JEDNAKIH
; -------------------------------------
L_NASTAVAK_LOOPA: MOV A, BROJAC	;
	SUB A, 1	    ; oduszmi 1 od A odnosno od BROJACa
	MOV BROJAC, A	; Nakon obrade vracam rezultat u BROJAC, umanjen za jedan sada.

	JZ LAVGDIO	    ; ako je brojac stigao do nule, JUMP ZERO me prebacuje na
			        ; racunanje srednje vrednosti jer sam sada prosao kroz sve clanove
			        ; niza i nemam vise brojaca, nema sta brojati.

	ADD AP, 1	    ; UVECAVAM ADRESU! OVO JE JAKO VAZNO! Ako bi adresu uvecao
			        ; Iznad prethodne linije, dakle da je bilo ADD AP, 1
			        ;                    					   JZ LAVGDIO
		         	; desi se nesto i JZ ne odreaguje na NULU U AKUMULATORU,
			 		; valjda JZ reaguje na promjenu u A ili AP, i u onom u kojem
					; je poslednja promjena bila, nju gleda, tako da ako AP uvecam
					; onda tu vise nije nula i indikator nule je aktivan ali ga
					; JZ ne pokupi i prodje dalje, udje u liniju ispod JMP LOOP i
					; vrti se jos nekoliko puta, u opstem slucaju bi vrtio
					; u BESKONACNU PETLJU ali ovde se dese neke slucajne okolnosti
					; pa stane kada nagura sumu na 120.
	JMP LOOP

; uvecavanje JEDNAKIH je ispod
L_INC_JEDNAKIH: MOV A, JEDNAKIH
	ADD A, 1
	MOV JEDNAKIH, A
	JMP L_NASTAVAK_LOOPA  	; VRATI SE BEZUOSLOVNO ODAKLE SI SKOCIO DO OVE LABELE
							; U ovom trenutku je uvecana vrednost JEDNAKIH i vracena u tu promenjivu

; uvecavanje promenjive koja cuva broj MANJIH
L_INC_MANJIH: MOV A, MANJIH
	ADD A, 1
	MOV MANJIH, A
	JMP L_NASTAVAK_LOOPA

;---------------------------------------
; U OVOM TRENUTKU ISPOD IMAM SRACUNATU SUMU
; AVG = SUM / 8
LAVGDIO: MOV A, SUM
	SHR
	SHR
	SHR
	MOV AVG, A
LEND: END
ENDPROGRAM
